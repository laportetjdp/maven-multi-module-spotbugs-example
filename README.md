# Maven Multi Module Spotbugs Example

Basic project that demonstrates the issue documented here: https://gitlab.com/gitlab-org/gitlab/-/issues/338786, 
where the current Gitlab Spotbugs process does redundant building and scanning of multi module maven projects.
