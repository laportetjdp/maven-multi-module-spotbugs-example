package passwordbug;
import java.sql.*;

public class PasswordBug {

    private static final String DB_PASSWORD = "password";

    public static void main(String[] args) throws SQLException {
        DriverManager.getConnection("test:3306", "test", DB_PASSWORD);
    }
}
