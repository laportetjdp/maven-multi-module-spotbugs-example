package injectionbug;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InjectionBug {
    public static void main(String[] args) throws SQLException {
        final Connection connection = DriverManager.getConnection("test:3306");
        final String query = String.join(",", args);
        final Statement statement = connection.createStatement();
        statement.executeQuery(query);
    }
}
